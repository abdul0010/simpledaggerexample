package uniapply.com.simpleDaggerExample;

public class Wheel {

    private Rims rimsl;
    private Tires tires;

    public Wheel(Rims rimsl, Tires tires) {
        this.rimsl = rimsl;
        this.tires = tires;
    }
}
